save() {
  DAY=`date "+%Y-%m-%d"`
  _tmp=`git log -1 --pretty=%B|grep "Day $DAY"`
  git add .
  if [ -z "$_tmp" ]
  then
    git commit -m "Day $DAY"
  else
    git commit --amend -m "Day $DAY"
  fi
  git push -f
}
# How To

## Retrieve sizes in postgress database ?

Get size of schema:

```sql
select schemaname, pg_size_pretty(sum(pg_total_relation_size(quote_ident(schemaname) || '.' || quote_ident(tablename)))::bigint) from pg_tables group by schemaname
```

Get size of all tables: 

```sql
select schemaname, tablename, pg_size_pretty(pg_total_relation_size(quote_ident(schemaname) || '.' || quote_ident(tablename))) from pg_tables order by schemaname
```

## Have a folder ignored by Git without changing .gitignore

Two ways:
- Add its path to `.git/info/exclude`
- Put a file `.gitignore` inside the folder with a single line `*`

## Have lines or blocks of code ignored from code coverage

Use `# pragma: no cover` on the same line (or first block line with `:`).
# HTTP Basic Auth

From https://nordicapis.com/the-difference-between-http-auth-api-keys-and-oauth/:
> HTTP Basic Auth is a simple method that creates a username and password style authentication for HTTP requests. This technique uses a header called Authorization, with a base64 encoded representation of the username and password. Depending on the use case, HTTP Basic Auth can authenticate the user of the application, or the app itself.
>
> A request using basic authentication for the user daniel with the password password looks like this:
>
> ```http
> GET / HTTP/1.1
> Host: example.com
> Authorization: Basic ZGFuaWVsOnBhc3N3b3Jk
> ```
> 
> When using basic authentication for an API, this header is usually sent in every request. The credentials become more or less an API key when used as authentication for the application. Even if it represents a username and password, it’s still just a static string.
>
> In theory, the password could be changed once in a while, but that’s usually not the case. As with the API keys, these credentials could leak to third parties. Granted, since credentials are sent in a header, they are less likely to end up in a log somewhere than using a query or path parameter, as the API key might do.
> 
> Using basic authentication for authenticating users is usually not recommended since sending the user credentials for every request would be considered bad practice. If HTTP Basic Auth is only used for a single request, it still requires the application to collect user credentials. The user has no means of knowing what the app will use them for, and the only way to revoke the access is to change the password.

In the example, the string `ZGFuaWVsOnBhc3N3b3Jk` could be obtained in python with:

```python
import base64
base64.b64encode("daniel:password".encode("utf-8")).decode()
```

Note that you could also use `printf "daniel:password" | base64`.
# Blockchain Development

## Learning Materials

- [How to Become a DeFi Developer - DeFi Weekly](https://defiweekly.substack.com/p/how-to-become-a-defi-developer) ⏳ 12m
- https://cryptozombies.io/fr/
- [How to build a full stack decentralized app - YouTube](https://www.youtube.com/watch?v=QAO7YxF7hSk) ⏳ 1:34:36
- [Develop a DeFi Project Using Python](https://blog.chain.link/develop-python-defi-project/) ⏳ 8m
- [Estimating gas price using pending transactions in Python explained - step-by-step beginners guides | QuikNode](https://www.quiknode.io/guides/web3-sdks/estimating-gas-price-using-pending-transactions-in-python) ⏳ 2m
- [Using the Binance Smart Chain with Python](https://blog.dendory.ca/2021/03/using-binance-smart-chain-with-python.html) ⏳ 3m

## Libraries and Tooling

- [maticnetwork/matic.js: Javascript developer library to interact with Matic Network](https://github.com/maticnetwork/matic.js) ⏳ 17m
- [Introduction — Web3.py 5.18.0 documentation](https://web3py.readthedocs.io/en/stable/)

## Pancakeswap Dev

### How to read pool data

The farming pool staking contract can be read at https://bscscan.com/address/0x73feaa1ee314f8c655e354234017be2193c9e24e#readContract

Each pool has an index, and pool info can be queried with the poolInfo field (9 in list).

As an exemple, the 132 index input gives:

- lpToken   address :  0x6Ff75C20656A0E4745E7c114972D361F483AFa5f
- allocPoint   uint256 :  200
- lastRewardBlock   uint256 :  6647315
- accCakePerShare   uint256 :  16725869935

The contract of the farming pool can be view at https://bscscan.com/address/0x6Ff75C20656A0E4745E7c114972D361F483AFa5f

And its token tracker is https://bscscan.com/token/0x6Ff75C20656A0E4745E7c114972D361F483AFa5f : same address

Tip: on top of the contract page of the farming pool, a label indicate the asset. Here is it "PancakeSwap: Suter".


On the staking contract, we can see the balance of LP token staked for each liquidity pool at the top, with the "Token" dropdown.

Another way to see this is to consult https://bscscan.com/tokenholdings?a=0x73feaa1ee314f8c655e354234017be2193c9e24e

The token list can be hard to read because most of LP tokens are just "Pancake LPs". However, the address of the token is also indicated, so we can know exactly what token it is.

'accCakePerShare': Seems to be the accumulated cake of each LP ? Hard to say like that.
'allocPoint': The number of cake given per block when divided by 100. For exemple, the pool of the cake token is 1 (https://bscscan.com/address/0xA527a61703D82139F8a06Bc30097cC9CAA2df5A6) and allocPoint is 4000 => 40 CAKE per block.

So in theory:

- The total value of a liquidity pool can be obtained from the contract page
  - https://bscscan.com/address/0x6Ff75C20656A0E4745E7c114972D361F483AFa5f
  - holdings => https://bscscan.com/tokenholdings?a=0x6Ff75C20656A0E4745E7c114972D361F483AFa5f
  - => "Value in USD", "Value in BNB", and quantity of each token
- The total supply of LP token can be obtained from https://bscscan.com/token/0x6Ff75C20656A0E4745E7c114972D361F483AFa5f
  - => "Total Supply"
- We can get the value of a LP token by dividing "total supply" by "value in USD"
  - => Then we can obtain the number of LP token of someone with the filter page: https://bscscan.com/token/0x6Ff75C20656A0E4745E7c114972D361F483AFa5f?a=0xf323cc25b029362c2dbf461720e060957f4307cc
  - And compute the amount of USD he has on the pool from this
- However, what we want is to know the amount of USD a given address A win the the FARMING pool
- For that we need to know the number of token of A in the pool relative to the total of the pool
  - We can query the amount of LP token for A in the pool with the 'userInfo' field, for exemple for pool 132 and A=0x49a5492FDFe5AcC966dD5f41310dfDfe8dAA349C:
    - amount   uint256 :  85623162172109843451
    - rewardDebt   uint256 :  1423812049699260622
  - Dividing the amount by 100 gives us the number of LP token of A, here: 85.623162172109843451
- We have the amount of LP token of A, and the total amount of LP token of the staking contract
  - The the number of of LP token of the staking contract, we can use the filter page of the token page: https://bscscan.com/token/0x6Ff75C20656A0E4745E7c114972D361F483AFa5f?a=0x73feaa1ee314f8c655e354234017be2193c9e24e
  - The staking contract is 0x73feaa1ee314f8c655e354234017be2193c9e24e in the URL
  - It's total balance in LP token can be obtained from the page under 'BALANCE'
  - By dividing both, we get the share of A on the farming pool
  - We know the amount of cake per block and the share in the pool, so we know how many cake we win per block
  - The number of block per day can be seen here https://bscscan.com/chart/blocks
  - So we have the number of cake we win per day on the pool

Using all of this, we can compute the APY over each pool at a given instant. If this cover potential risk (assets variation, impermanent loss), then its a win. Also it allows us to get compound interest of reward, and diversification.

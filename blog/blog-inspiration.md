# Bloc Inspiration
*Examples of blogs for which I like the design and UX*

**[Stargirl (Thea) Flowers - Blog](https://blog.thea.codes/)**

Example of blog post: https://blog.thea.codes/we-need-to-talk-about-github/

- + Clean minimal design
- + Good colors
- + Good typo
- + RSS
- + Timeline and dated articles
- + Social links
- + No header, but footer instead
- - No tags
- - No series
- - No bundling per year
- - No reading time
- - No ToC
- - Links in main text

Note that her personal page is quite great too: https://thea.codes/
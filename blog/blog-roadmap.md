# Blog / Roadmap

## MVP

- Start with a Hugo blog, standard theme, no fancy things
- At minima: RSS and comments (using utterance)
- Bonus: date, sort by year, tags, reading time, multi-language

This is a MVP, the goal is to test my capacity to publish content regularly.

After enough time, and when I can and I have time, write my own static generator and integrate the blog in a more general knownledge management system.

## Post Ideas

- Knowledge management, mind palace for the developer

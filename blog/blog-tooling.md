# Blog Tooling

## Tools

- [bebraw/pypandoc: Thin wrapper for "pandoc" (MIT)](https://github.com/bebraw/pypandoc)
- [theacodes/cmarkgfm: Python bindings for GitHub's cmark](https://github.com/theacodes/cmarkgfm)
- [eyeseast/python-frontmatter: Parse and manage posts with YAML (or other) frontmatter](https://github.com/eyeseast/python-frontmatter)

## Guides

- [Stargirl (Thea) Flowers - Writing a small static site generator](https://blog.thea.codes/a-small-static-site-generator)
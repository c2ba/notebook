# Guide to a good VS code project hosted on Git

- Ensure all developers will use same code conventions
  - [Use a `.gitattributes` file to ensure line ending](https://www.aleksandrhovhannisyan.com/blog/crlf-vs-lf-normalizing-line-endings-in-git/)
  - Share editor configuration (`.vscode` folder)
    - What if someone really want a custom `.vscode` ? Share it under `.devenv/.vscode` and let developers use symbolic links for the sharing
- Put developer tooling in a dedicated `.devenv` folder
  - Have a `.devenv/bash_init.sh` script that should be used to init any bash session started in the project
    - Developers can manually source it when starting a new terminal
    - Or they can add `[[ -f .devenv/bash_init.sh ]] && source .devenv/bash_init.sh` in their `.bashrc` or `.bash_profile` file to auto load source it when a terminal is started in the project folder.


I love open source. But I don't qualify myself as an "open source developer". Not yet. It's strange to say that because most of my projects have been open sourced. Both personal and professional projects.
But what I really want is to spend all my work time on open source development. Only working with passion on projects I love and are useful to me.
I truly believe open source is the future of programming, and that developers should be able to only work open source, in pure freedom.

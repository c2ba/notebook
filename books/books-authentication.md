# Books - Authentication

# Authentication

## Advanced API Security - Securing APIS with OAuth 2.0, OpenID Connect, JWS and JWS

![](pictures/advanced_api_security.jpg)

- pages: 248
- apress

## JWT Handbook

![](pictures/jwt_handbook.jpg)

- pages: 120
- javascript
- auth0

## Mastering OAuth 2.0

![](pictures/mastering_oauth_2.jpg)

- pages: 238
- packt

## OAuth 2 in Action

![](pictures/oauth2_in_action.jpg)

- pages: 362
- javascript
- manning

## OAuth 2.0 Identity and Access MAnagement Patterns

![](pictures/oauth2_identity_and_access_management_patterns.jpg)

- pages: 129
- packt

## OpenSSL Cookbook

![](pictures/openssl_cookbook.jpg)

- pages: 57

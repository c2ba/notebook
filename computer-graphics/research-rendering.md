# Computer Graphics / Research / Rendering

- [NVIDIAGameWorks/Falcor: Real-Time Rendering Framework](https://github.com/NVIDIAGameWorks/Falcor) ⏳ 4m
- [Nori: an educational ray tracer](https://wjakob.github.io/nori/)

## Linearly Transformed * for Sampling

- http://momentsingraphics.de/LinearlyTransformedSH.html
- [Integrating Clipped Spherical Harmonics Expansions | ACM Transactions on Graphics](https://dl.acm.org/doi/10.1145/3015459)
- [Real-time polygonal-light shading with linearly transformed cosines | ACM Transactions on Graphics](https://dl.acm.org/doi/10.1145/2897824.2925895)
- http://www.jallmenroeder.de/2020/11/19/linearly-transformed-spherical-harmonics/
- [Linearly Transformed Spherical Harmonic Expansions](http://www.jallmenroeder.de/wp-content/uploads/2020/10/LTSH_BA_Thesis_final.pdf)

# C/C++ Ecosystem

- [Awesome hpp: A curated list of awesome header-only C++ libraries](https://github.com/p-ranav/awesome-hpp)
- [cpplint/cpplint: Static code checker for C++](https://github.com/cpplint/cpplint)
- [lucteo/concore: Core abstractions for dealing with concurrency in C++](https://github.com/lucteo/concore)
  - See article https://accu.org/journals/overload/28/159/teodorescu/
- [Clang-Tidy — Extra Clang Tools 12 documentation](https://clang.llvm.org/extra/clang-tidy/)
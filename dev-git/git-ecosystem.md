# Dev / Git / Ecosystem

- [IonicaBizau/git-stats: 🍀 Local git statistics including GitHub-like contributions calendars.](https://github.com/IonicaBizau/git-stats)
- [tj/git-extras: GIT utilities -- repo summary, repl, changelog population, author commit percentages and more](https://github.com/tj/git-extras)
  - [4 Git scripts I can't live without | Opensource.com](https://opensource.com/article/20/4/git-extras) ⏳ 3m
- [hub · an extension to command-line git](https://hub.github.com/) ⏳ 2m

## Encrypted Git

- [4 secrets management tools for Git encryption | Opensource.com](https://opensource.com/article/19/2/secrets-management-tools-git) ⏳ 6m

## Self Hosted Git

- [Gitea](https://gitea.io/en-us/)
- [Gitolite](https://gitolite.com/gitolite/) ⏳ 3m

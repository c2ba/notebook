Après le split, le merge de deux fichiers.

La technique est similaire, on va exploiter un merge git pour combiner les deux version. Il faudra par contre utiliser la commande git cat-file pour finaliser la combinaison.


Supposons qu'on veuille merge A et B en un seul nouveau fichier C. On est sur ma_branche initialement.

On commence par renommer A en C puis on commit sur une branche _tmp:
git mv A C
git checkout -b _tmp
git commit -m "refactor: rename A -> C



On se remet sur ma_branche puis on renomme B en C, on commit et on tente de merger _tmp

git checkout ma_branche

git mv B C

git commit -m "refactor: rename B -> C

git merge _tmp



Comme pour le split, git nous annonce un conflit ici. On utilise la commande git cat-file pour créer le nouveau fichier C final:

git cat-file --filters _tmp:C >C # copie A (qui a été renommé C sur _tmp) et son historique au debut de C

git cat-file --filters HEAD:C >>C # copie B (qui a été renommé C sur HEAD) et son historique a la fin de C
git add C

git merge --continue # on peut aussi faire un git commit -m "refactor: finalize merge C <- A + B" si on veut un message plus clair



Voilà, ensuite il faut généralement aller re-ordonner les choses dans C car on veut rarement juste une concaténation des deux fichiers.



Reference: https://devblogs.microsoft.com/oldnewthing/20190514-00/?p=102493 (ce type donne des vrai techniques de techos en Git)

L'historique d'un fichier c'est important dans Git, ça permet de connaitre la chaine de commit qui ont mené à l'existence de ce fichier dans son état actuel (et avec un bon éditeur de savoir très rapidement quel est la dernière personne à avoir touché à chaque ligne).

Il arrive qu'on veuille couper en deux un fichier lors d'un refactoring, comment ne pas perdre l'historique de ce fichier ? Il faut jouer un peu avec les merge. Voici la procédure:

On suppose qu'on est sur la branche ma_branche et qu'on veut couper le fichier A en B et C. On commence par renommer A en B avec git:
git mv A B
Puis on créé une branche temporaire sur laquelle on commit ce changement:
git checkout -b _tmp
git commit -m "refactor: rename A -> B"
On retourne ensuite sur la branche d'origine sur laquelle A existe toujours:
git checkout ma_branche
On le renomme cette fois ci en C puis on commit:

git mv A C

git commit -m "refactor: rename A -> C"

Arrivé ici on a B sur la branche _tmp et C sur la branche courante ma_branche

On merge donc _tmp pour récupérer B.

git merge _tmp



Et la c'est la panique car git nous annonce 3 conflits sur les 3 fichiers. Heureusement c'est un leure, il suffit de stager les modification sur les 3 fichiers puis de finir le merge:

git add A B C

git commit -m "refactor: merge renaming"



On peut vérifier que les fichiers B et C contiennent bien l'historique de A en utilisant les commandes suivantes:

git log --follow B

git log --follow C

L'option follow est importante pour suivre les renommage de fichier dans l'historique.



Pour finir la procédure on supprime la branche temporaire produite:

git branch -d _tmp



Il suffit ensuite de supprimer tout ce qu'on veut dans B et dans C pour faire le split.



Et si on veut simplement extraire une sous partie de A dans B ? Il suffit de faire la procédure puis de renommer C en A ensuite (il y a peut être plus court mais pas essayé, et je pense qu'il faut vraiment que A n'existe plus quand on fait le merge, sinon le merge le supprimera j'imagine, à essayer).

# Dev / Git / Learning Materials

- [Managing Git projects with submodules and subtrees | Opensource.com](https://opensource.com/article/20/5/git-submodules-subtrees) ⏳ 5m
  - **TL;DR** A submodule is a link and a commit in another repository; A subtree is a history of commits for a subfolder
- [Getting started with Git: Terminology 101 | Opensource.com](https://opensource.com/article/19/2/git-terminology) ⏳ 7m
- [How to write custom Git hooks and publishing your code to a website | Opensource.com](https://opensource.com/life/16/8/how-construct-your-own-git-server-part-6) ⏳ 11m
- [How to manage binary assets with Git | Opensource.com](https://opensource.com/life/16/8/how-manage-binary-blobs-git-part-7) ⏳ 7m
- [Run a server with Git | Opensource.com](https://opensource.com/article/19/4/server-administration-git) ⏳ 9m
- [Tweak your Git config for multiple user IDs | Opensource.com](https://opensource.com/article/20/10/git-config) ⏳ 2m
- [CRLF vs. LF: Normalizing Line Endings in Git | Aleksandr Hovhannisyan](https://www.aleksandrhovhannisyan.com/blog/crlf-vs-lf-normalizing-line-endings-in-git/) ⏳ 18m

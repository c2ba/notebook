# Dev / Git / Workflows

## [Git : comment nommer ses branches et ses commits ? - Code Heroes](https://www.codeheroes.fr/2020/06/29/git-comment-nommer-ses-branches-et-ses-commits/)

**TL;DR**

`<type>/<name>/<issue_ID>`

Types:

- feature: Ajout d’une nouvelle fonctionnalité;
- bugfix: Correction d’un bug;
- hotfix: Correction d’un bug critique;
- chore: Nettoyage du code;
- experiment: Expérimentation de fonctionnalités.

Nommer les commits, utiliser conventional-commit convention:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Types:

- feat: Ajout d’une nouvelle fonctionnalité;
- fix: Correction d’un bug;
- build: Changement lié au système de build ou qui concerne les dépendances (npm, grunt, gulp, webpack, etc.).
- ci: Changement concernant le système d’intégration et de déploiement continu (Jenkins, Travis, Ansible, gitlabCI, etc.)
- docs: Ajout ou modification de documentation (README, JSdoc, etc.);
- perf: Amélioration des performances;
- refactor: Modification n’ajoutant pas de fonctionnalités ni de correction de bug (renommage d’une variable, suppression de code redondant, simplification du code, etc.);
- style: Changement lié au style du code (indentation, point virgule, etc.);
- test: Ajout ou modification de tests;
- revert: Annulation d’un précédent commit;
- chore: Toute autre modification (mise à jour de version par exemple).

Scope: composants de notre projet

Sujet:

- Le sujet doit faire moins de 50 caractères;
- Les verbes doivent être à l’impératif (add, update, change, remove, etc.);
- La première lettre ne doit pas être en majuscule;
- Le sujet ne doit pas se terminer par un point.

Conventions:

- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [Semantic Versioning 2.0.0 | Semantic Versioning](https://semver.org/)
- [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Tooling:

- [conventional-changelog/commitlint: 📓 Lint commit messages](https://github.com/conventional-changelog/commitlint)
- [commitizen/cz-cli: The commitizen command line utility. #BlackLivesMatter](https://github.com/commitizen/cz-cli)
- [semantic-release/semantic-release: Fully automated version management and package publishing](https://github.com/semantic-release/semantic-release)
- [standard-version  -  npm](https://www.npmjs.com/package/standard-version)

## [Emoji-Log: A new way to write Git commit messages | Opensource.com](https://opensource.com/article/19/2/emoji-log-git-commit-messages) ⏳ 4m

- [gitmoji | An emoji guide for your commit messages](https://gitmoji.carloscuesta.me/)

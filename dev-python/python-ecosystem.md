# Python Ecosystem

## Static Analysis

- [PyCQA/mccabe: McCabe complexity checker for Python](https://github.com/PyCQA/mccabe)

## Code Formatting

- [psf/black: The uncompromising Python code formatter](https://github.com/psf/black) ⏳ 12m

## Packaging

- [Python Packaging Authority — PyPA  documentation](https://www.pypa.io/en/latest/)

## Dependency Management

- [Poetry - Python dependency management and packaging made easy.](https://python-poetry.org/)
- [pip-tools · PyPI](https://pypi.org/project/pip-tools/) ⏳ 6m
- [Pipenv: Python Dev Workflow for Humans — pipenv 2020.11.16.dev0 documentation](https://pipenv.pypa.io/en/latest/) ⏳ 3m

## CLI

- [Typer](https://typer.tiangolo.com/) ⏳ 4m
- [Rich - a Python library for writing rich text (with color and style) to the terminal](https://rich.readthedocs.io/en/latest/index.html)

## Automation

- [Welcome to the tox automation project — tox 3.20.2.dev22 documentation](https://tox.readthedocs.io/en/latest/) ⏳ 4m
- [Welcome to Nox — Nox 2020.8.22 documentation](https://nox.thea.codes/en/stable/) ⏳ 1m

## Code Metrics

- [Understand your Python code with this open source visualization tool | Opensource.com](https://opensource.com/article/20/11/python-code-viztracer) ⏳ 2m

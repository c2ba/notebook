# Dev / Python / Good Practices

- Prefix with `_` private elements
  - Private to module
  - Private to class

- Use module level imports instead of function level imports
  - Allow mocking

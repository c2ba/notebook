# Dev / Python / Learning Materials

## HTTP

- [The modern way to call APIs in Python](https://kernelpanic.io/the-modern-way-to-call-apis-in-python) ⏳ 9m

## Cloud Architectures in Python

- [Python Microservices With gRPC – Real Python](https://realpython.com/python-microservices-grpc/) ⏳ 1h25m

## Testing in Python

If writing a learning path, to forget to include mock and pytest documentation. Selenium also seems to be important for web applications testing.

- https://wiki.python.org/moin/PythonTestingToolsTaxonomy
- [End-To-End Tutorial For Pytest Fixtures With Examples](https://www.lambdatest.com/blog/end-to-end-tutorial-for-pytest-fixtures-with-examples/) ⏳ 27m
  - To be reviewed quickly, I think I already know most of the things mentionned here

- [Lisa Roach - Demystifying the Patch Function   - PyCon 2018 - YouTube](https://www.youtube.com/watch?v=ww1UsGZV8fQ) ⏳ 37:06
  - This one is probably a must watch about testing

### Reading List

- [ ] [Spying on instance methods with Python's mock module - Wes McKinney](https://wesmckinney.com/blog/spying-with-python-mocks/)
- [ ] [Subtests in Python](https://blog.ganssle.io/articles/2020/04/subtests-in-python.html) ⏳ 18m
- [ ] [Given-When-Then - Python Testing](https://pythontesting.net/strategy/given-when-then-2/) ⏳ 15m
- [ ] [Using the Python mock library to fake regular functions during tests](https://fgimian.github.io/blog/2014/04/10/using-the-python-mock-library-to-fake-regular-functions-during-tests/) ⏳ 5m
- [ ] [pytest: How to mock in Python – Chang Hsin Lee – Committing my thoughts to words.](https://changhsinlee.com/pytest-mock/) ⏳ 9m

## Database Mocking

- [IDisposable Thoughts - Testing SQLAlchemy with SQLite in memory and&nbsp;schemas](https://cprieto.com/posts/2019/02/testing-sqlalchemy-with-sqlite-in-memory-and-schemas.html) ⏳ 3m
  - For database testing, using SQLite in memory
  - However, a good alternative could be to spawn a container before running the tests (can be done from python). It requires docker, but you can use the DB you want, and its closer to integration.
  - Also SQLAlchemy seems to provide https://docs.sqlalchemy.org/en/14/core/engines.html#sqlalchemy.create_mock_engine
- [Database Testing with pytest - YouTube](https://www.youtube.com/watch?v=c9oeoN1AnUM) ⏳ 26:29
- [How do I mock a database? Pytest Mocking Tutorial - YouTube](https://www.youtube.com/watch?v=zHgXRlUBA5A) ⏳ 7:55

## Mocking with pytest-mock

- [Mocking functions with Pytest-mock Part I | Analytics Vidhya](https://medium.com/analytics-vidhya/mocking-in-python-with-pytest-mock-part-i-6203c8ad3606) ⏳ 7m
- [Mocking Functions Part II | Better Unit testing in Python | Medium](https://medium.com/@durgaswaroop/writing-better-tests-in-python-with-pytest-mock-part-2-92b828e1453c) ⏳ 4m

### [Understanding the Python Mock Object Library – Real Python](https://realpython.com/python-mock-library/) ⏳ 31m

A realpython course about mocking in python. Probably worth investing time in it. Target unittest and not pytest, but same concepts.

### [Python Testing - Python Software Development and Software Testing (posts and podcast)](https://pythontesting.net/)

A complete website dedicated to testing in Python, but many parts apply to any language. Link to the book `Python Testing with pytest` (256 pages).

### [Web UI Testing Made Easy with Python, Pytest and Selenium WebDriver | TestProject](https://blog.testproject.io/2019/07/16/web-ui-testing-python-pytest-selenium-webdriver/) ⏳ 3m

A 7 chapters tutorial about testing Web UI using Python. Seems great.

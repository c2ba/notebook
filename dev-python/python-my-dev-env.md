# Dev / Python / My Dev Env

## Tooling and env

- Windows
- python installed with pyenv-win, several versions
- pipx installed to be able to quickly install python tools
- using python -m venv to create virtuals envs
- using poetry for medium / large projects
- poetry installed with recommended installer: allow to use several versions of python
- using pip-tools for small projects
- pip-tools should be installed in the project venv
- my venvs are in $project/.venv
- venv_enter / venv_exit
- VSCode to develop
- PYTHON_EXE_PATH set from env var
- bashrc hook
- using docker for complex infrastructure
- using cookiecutter for project templates
- using PIP_REQUIRE_VIRTUALENV=true
- using pre-commit
- using nox
- poetry, nox and pre-commit should be installed outside of project venv.
  - Why ?
    - poetry install dependencies into project env
    - but nox jobs call poetry to fill its own venvs, so nox cannot be a poetry dependency
    - pre-commit uses poetry run for some checks, so it cannot be a poetry dependency

## TDD in python

- pytest, pytest-cov, pytest-mock, pytest-subtests
- given when then:

```python
def given(subtests: SubTests, CONTEXT):
    # WHEN
    with subtests.test(msg="bar"):
        # THEN
```

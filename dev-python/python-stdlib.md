# Python / Standard Library

## [A complete guide for working with I/O streams and zip archives in Python 3 | by Naren Yellavula | Dev bits | Medium](https://medium.com/dev-bits/ultimate-guide-for-working-with-i-o-streams-and-zip-archives-in-python-3-6f3cf96dca50) ⏳ 7m

This guide gives details about I/O Streams, both text and binary, as well as the `zipfile` module that provides a high level interface on top of I/O binary streams.

It shows various common use cases related to the subject and provides code for it.

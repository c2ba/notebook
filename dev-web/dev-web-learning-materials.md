# Dev / Web / Learning Materials
*Learning materials stored by concepts that I find online, see [this document](dev-web-learning-materials.md) to follow my progress on some of them*

## Authentication

- [Demystifying authentication with FastAPI and a frontend](https://kernelpanic.io/demystifying-authentication-with-fastapi-and-a-frontend) ⏳ 11m
- [Securing FastAPI with JWT Token-based Authentication | TestDriven.io](https://testdriven.io/blog/fastapi-jwt-auth/) ⏳ 9m
- [Web Authentication and Authorization Reference Guide | thedeployguy](https://thedeployguy.com/2020-07-12-web-auth-cheatsheet/)

## Encryption

In python

- https://nitratine.net/blog/post/how-to-hash-passwords-in-python/
- https://nitratine.net/blog/post/python-encryption-and-decryption-with-pycryptodome/
- https://nitratine.net/blog/post/encryption-and-decryption-in-python/
- https://www.devdungeon.com/content/symmetric-encryption-python-using-fernet-aes

General theory

- https://nordpass.com/blog/password-salt/
- https://flylib.com/books/en/1.274.1/message_digests_macs_and_hmacs.html

## Webpack

Tutorial Series "Getting to know Webpack in 2020 - Beginner friendly introduction | Part 1,2,3"
> Webpack is a very powerful tool, most modern web development are using webpack to bundle their applications. If you have ever used create react app well that uses webpack behind the scenes to bundle its code, it mostly hides this fact from you unless you eject from it.
- [x] ~~*https://thedeployguy.com/2020-06-07-getting-to-know-webpack/*~~ [2020-12-08]
- [x] ~~*https://thedeployguy.com/2020-06-14-adding-typescript-to-web/*~~ [2020-12-08]
- [ ] https://thedeployguy.com/2020-06-21-production-ready-webpack/

- [webpack Tutorial: How to Set Up webpack 5 From Scratch](https://www.taniarascia.com/how-to-use-webpack/) (2020)

- [Setting up a React + TypeScript + SASS + Webpack and Babel project in 6 Steps](https://medium.com/swlh/setting-up-a-react-typescript-sass-webpack-and-babel-7-project-in-6-steps-b4d172d1d0d6) (2019, Pay)
- [Setting TypeScript For Modern React Projects Using Webpack](https://www.smashingmagazine.com/2020/05/typescript-modern-react-projects-webpack-babel/) (2020, Free)

- [Development Servers Compared: Webpack-Dev-Server Vs. Webpack-Serve | by Jeff Lewis | Medium](https://medium.com/@jeffrey.allen.lewis/development-servers-compared-webpack-dev-server-vs-webpack-serve-no-its-not-missing-an-r-745fc5f78c0a)
- [Webpack dev server, Hot reload and source maps — Easing the pain. | by Ahmad Asaad | Medium](https://medium.com/@Ahmad.Asaad/webpack-dev-server-hot-module-replacement-hmr-and-source-maps-easing-the-pain-e7cee99e3bdf)
- [How to build a React project from scratch using Webpack 4 and Babel | Hacker Noon](https://hackernoon.com/how-to-build-a-react-project-from-scratch-using-webpack-4-and-babel-56d4a26afd32)

## React

- [React App From Scratch](https://marlom.dev/react-app-from-scratch) ⏳ 10m
- 💎 [React's useEffect and useRef Explained for Mortals | Strings and Things](https://leewarrick.com/blog/react-use-effect-explained/) ⏳ 12m
- [Learn React in 1 Hour by Building a Movie Search App](https://www.freecodecamp.org/news/learn-react-in-1-hour-by-building-a-movie-search-app/) ⏳ 5m
- [mpolinowski/react-under-the-hood: A look behind the curtain of React Starters](https://github.com/mpolinowski/react-under-the-hood) ⏳ 18m
- [How to avoid multiple re-renders in React (3 lines of code)](https://linguinecode.com/post/how-to-avoid-multiple-re-renders-in-react-shouldcomponentupdate)
- [Avoiding React setState() Pitfalls – Duncan Leung](https://duncanleung.com/avoiding-react-setstate-pitfalls/)
- [Myths about useEffect | Epic React by Kent C. Dodds](https://epicreact.dev/myths-about-useeffect/)
- [React Today and Tomorrow and  90% Cleaner React With Hooks - YouTube](https://www.youtube.com/watch?v=dpw9EHDh2bM&feature=youtu.be&t=643) ⏳ 1:35:29
- [componentDidMakeSense — React Component Lifecycle Explanation | by Trey Huffine | Level Up Coding](https://levelup.gitconnected.com/componentdidmakesense-react-lifecycle-explanation-393dcb19e459) ⏳ 4m
- [The Problem with React's Context API | Strings and Things](https://leewarrick.com/blog/the-problem-with-context/)
- [How to get the url params from a route in React | Reactgo ](https://reactgo.com/react-get-url-params/)
- [How to use the useParams hook in React router | Reactgo ](https://reactgo.com/react-router-useparams-hook/)
- [How to use the useHistory hook in React router | Reactgo ](https://reactgo.com/react-router-usehistory-hook/)
- [react route pass props using useHistory Code Example](https://www.codegrepper.com/code-examples/fortran/react+route+pass+props+using+useHistory)
- For undo/redo, not router: [useHistory React Hook - useHooks](https://usehooks.com/useHistory/) ⏳ 3m
- [The way to check if it’s the first time for useEffect function is being run in React Hooks | by Anna Coding | Anna Coding | Medium](https://medium.com/anna-coding/the-way-to-check-if-its-the-first-time-for-useeffect-function-is-being-run-in-react-hooks-170520554067)

## Flask + React

- [React-Flask Integration: Part 1 - Setup with Webpack - DEV](https://dev.to/icewreck/react-flask-integration-part-1-setup-with-webpack-djo)
- [Serve React with Flask: How to Serve a React-app With a Flask-Server -](https://blog.learningdollars.com/2019/11/29/how-to-serve-a-reactapp-with-a-flask-server/)

## Flask + Websocket

- [Weekend Project (Part 1): Creating a Real-time Web-based Application using Flask, Vue, and Socket.io | by Josh Porter | Hacker Valley Studio | Medium](https://medium.com/hackervalleystudio/weekend-project-part-1-creating-a-real-time-web-based-application-using-flask-vue-and-socket-b71c73f37df7) ⏳ 1m

## Typescript

1. [TypeScript Tutorial for JS Programmers Who Know How to Build a Todo App](https://ts.chibicode.com/todo/)
2. [Understanding TypeScript’s type notation](https://2ality.com/2018/04/type-notation-typescript.html)
3. [GitHub - typescript-cheatsheets/react: Cheatsheets for experienced React developers getting started with TypeScript](https://github.com/typescript-cheatsheets/react)

- [The --strict Compiler Option in TypeScript — Marius Schulz](https://mariusschulz.com/blog/the-strict-compiler-option-in-typescript) ⏳ 2m
- [TypeScript Evolution — Marius Schulz](https://mariusschulz.com/blog/series/typescript-evolution)

## Typescript + Webpack

- [I CAN MAKE THIS WORK...: Using TypeScript and ESLint with webpack (fork-ts-checker-webpack-plugin new feature!)](https://blog.johnnyreilly.com/2019/07/typescript-and-eslint-meet-fork-ts-checker-webpack-plugin.html) ⏳ 4m

**Tools**

- [ts-loader  -  npm](https://www.npmjs.com/package/ts-loader) ⏳ 11m
- [GitHub - TypeStrong/fork-ts-checker-webpack-plugin: Webpack plugin that runs typescript type checker on a separate process.](https://github.com/TypeStrong/fork-ts-checker-webpack-plugin) ⏳ 7m

## Typescript + Babel

- [Setting up Babel and TypeScript](https://ricostacruz.com/til/babel-typescript) ⏳ 2m

## Typescript + React

- [How to Statically Type React Components with TypeScript | Pluralsight](https://www.pluralsight.com/guides/how-to-statically-type-react-components-with-typescript)
- [Composing React Components with TypeScript | Pluralsight](https://www.pluralsight.com/guides/composing-react-components-with-typescript)
- [Using AbortController (with React Hooks and TypeScript) to cancel window.fetch requests - DEV](https://dev.to/bil/using-abortcontroller-with-react-hooks-and-typescript-to-cancel-window-fetch-requests-1md4)

## React without ...

- [React without npm, Babel, or webpack | by Chris Lewis | Medium](https://medium.com/@chrislewisdev/react-without-npm-babel-or-webpack-1e9a6049714) ⏳ 4m
- [reactjs - React without Webpack - Stack Overflow](https://stackoverflow.com/questions/36609910/react-without-webpack)
- [GitHub - misterfresh/react-without-webpack: The lightest React Starter Kit, using native modules](https://github.com/misterfresh/react-without-webpack)
- [React without Webpack : faster developer workflow? | by Antoine Stollsteiner | Frontend Weekly | Medium](https://medium.com/front-end-weekly/react-without-webpack-a-dream-come-true-6cf24a1ff766) ⏳ 3m
- [Using ReactJS without Webpack - Jack Franklin](https://www.jackfranklin.co.uk/blog/react-no-webpack/) ⏳ 2m

## Redux

- [Redux Essentials, Part 1: Redux Overview and Concepts | Redux](https://redux.js.org/tutorials/essentials/part-1-overview-concepts)

## Web Automation

- [Modern Web Automation With Python and Selenium](https://realpython.com/modern-web-automation-with-python-and-selenium/)

## REST APIs

- [REST API Design - Resource Modeling | ThoughtWorks](https://www.thoughtworks.com/insights/blog/rest-api-design-resource-modeling) ⏳ 22m
- [The Law of Leaky Abstractions – Joel on Software](https://www.joelonsoftware.com/2002/11/11/the-law-of-leaky-abstractions/) ⏳ 11m
- [Designer une API REST | OCTO Talks !](https://blog.octo.com/designer-une-api-rest/) ⏳ 29m
- [Sécuriser une API REST : tout ce qu’il faut savoir | OCTO Talks !](https://blog.octo.com/securiser-une-api-rest-tout-ce-quil-faut-savoir/) ⏳ 32m
- [Concevoir une API REST conforme au RGPD | OCTO Talks !](https://blog.octo.com/concevoir-une-api-rest-conforme-au-rgpd/) ⏳ 16m

## Linting

- [ESLint - Pluggable JavaScript linter](https://eslint.org/)
- CSS and variants: [Stylelint](https://stylelint.io/)

## Formatting

**Why**

- [Prettier vs ESLint: What's The Difference?](https://www.futurehosting.com/blog/prettier-vs-eslint-whats-the-difference/) ⏳ 3m

**How**

- [How to use Prettier in VS Code - RWieruch](https://www.robinwieruch.de/how-to-use-prettier-vscode) ⏳ 3m
- [How to make Prettier work with ESLint - RWieruch](https://www.robinwieruch.de/prettier-eslint) ⏳ 4m
- [How to configure ESLint and Prettier to work together | ryanharris.dev](https://ryanharris.dev/configure-eslint-and-prettier/) ⏳ 3m
- [Prettier &amp; ESLint Setup for VSCode - DEV](https://dev.to/thetinygoat/prettier-eslint-setup-for-vscode-5b6) ⏳ 2m

**Tools**

- [Prettier · Opinionated Code Formatter](https://prettier.io/)

## Live Reload

- [4 Ways to Auto-Refresh your Browser when Designing new Sites](https://code.tutsplus.com/tutorials/4-ways-to-auto-refresh-your-browser-when-designing-new-sites--net-13299)

## Config Management

- [davidtheclark/cosmiconfig: Find and load configuration from a package.json property, rc file, or CommonJS module](https://github.com/davidtheclark/cosmiconfig) ⏳ 1m

## Tooling (General)

- [Javascript Build Tools](https://opensource.com/article/20/11/javascript-build-tools)
- Parcel, webpack alternative https://parceljs.org/getting_started.html ?
- [Storybook: UI component explorer for frontend developers](https://storybook.js.org/)

## Full Courses

- 💰 [Advanced React and GraphQL](https://courses.wesbos.com/account/access/5fc6e1fffff9391cecee78f0) course
  - slack: https://join.slack.com/t/wesbos/invite/enQtMTU2NTk4OTg3Mzg1Ni1iMzQxYTRiMzVkMjhjMjgyZjY2NGY0NjI3ODEyZGQ1ZGM3YTcwMWU2ZmQ0NGZlOGFhNzVkNDUzMzNlOTJmZTI4?x=x-p11237033681-11234408512-1539059818005
- 💰 [Modern APIs with FastAPI and Python - [Talk Python Training]](https://training.talkpython.fm/courses/details/getting-started-with-fastapi) ⏳ 3m
- [Flask by Example (Learning Path)](https://realpython.com/learning-paths/flask-by-example/)

## Security

- [Subresource Integrity - Web security | MDN](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity) ⏳ 5m

# Dev / Web / Test Driven Development

- The most used testing library seems to be [Jest](https://jestjs.io/)
- Under the hood Jest is using [jsdom/jsdom: A JavaScript implementation of various web standards, for use with Node.js](https://github.com/jsdom/jsdom)
  - It provides utilities to simulate a web browser in testing sessions
  - To run scripts we need to load DOM with: `dom = new JSDOM(html, { runScripts: 'dangerously' })`
- Another library used with Jest is [Testing Library | Testing Library](https://testing-library.com/)
  - The `@testing-library` family of packages helps you test UI components in a user-centric way.
  - It provides utilities to get elements from the page easily or interact with events.
  - It has integration with several other libraries such as React, Vue, Angular, Pupeeter, JSDOM, ...
- Specialized for React we also have [Enzyme](https://enzymejs.github.io/enzyme/)
- For end to end testing we can use frameworks like:
  - [JavaScript End to End Testing Framework | cypress.io](https://www.cypress.io/)
  - [Selenium](https://www.selenium.dev/)

## [How to Unit Test HTML and Vanilla JavaScript Without a UI Framework - DEV Community 👩‍💻👨‍💻](https://dev.to/thawkin3/how-to-unit-test-html-and-vanilla-javascript-without-a-ui-framework-4io) ⏳ 8m

This article explains how to use the most common JS testing libraries to test a simple HTML page with its embedded script.

## Jest with React

Note that with `react-scripts` version 4+ we need the following configuration in `package.json`:

```json
"jest": {
  "resetMocks": false,
}
```

See https://dev.to/hanvyj/comment/1c57m

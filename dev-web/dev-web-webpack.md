# Dev / Web / Webpack

## Learning Path
*See my annotations with hypothesis in group https://hypothes.is/groups/QNwj6jAA/c2ba*

**What I did so far**

- [Getting to know Webpack in 2020 - Beginner friendly introduction | Part 1 | thedeployguy](https://thedeployguy.com/2020-06-07-getting-to-know-webpack/#annotations:bKPY7DWPEeuihAu_UpS71A)
  - [Why webpack | webpack](https://webpack.js.org/concepts/why-webpack/)
  - Goal is to create by end a `webpack.config` file for a React application, so we understand what `create-react-app` is doing under the hood
- [Getting to know Webpack - How to add Typescript and CSS support | Part 2 | thedeployguy](https://thedeployguy.com/2020-06-14-adding-typescript-to-web/#annotations:U5jDjDZVEeuum7dW5ablDQ)
  - ~~Don't know how to solve type issues for now, need to search~~:
    - `npm install @types/react` solves:
      - Binding element 'welcomeMessage' implicitly has an 'any' type.ts(7031)
      - JSX element implicitly has type 'any' because no interface 'JSX.IntrinsicElements' exists.ts(7026)
    - Setting `"jsx": "react"` in `tsconfig.json` solves:
      - Cannot use JSX unless the '--jsx' flag is provided.ts(17004)

**Babel only transpiles Typescript**

- No type checking is done by Babel ([TypeScript: Documentation - Using Babel with TypeScript](https://www.typescriptlang.org/docs/handbook/babel-with-typescript.html#babel-for-transpiling-tsc-for-types) ⏳ 1m)
- This could also be done with `ts-loader` by activating `transpileOnly: true` ([ts-loader  -  npm](https://www.npmjs.com/package/ts-loader#faster-builds) ⏳ 11m)
- To typecheck, either run `tsc --noEmit` manually, or use [GitHub - TypeStrong/fork-ts-checker-webpack-plugin: Webpack plugin that runs typescript type checker on a separate process.](https://github.com/TypeStrong/fork-ts-checker-webpack-plugin) ⏳ 7m
  - [ ] Try this in my webpack tutorial project

**Things I understand so far**

- React can be used directly from Vanilla JS (html page)
- Babel can be used directly from html page to transpile JSX (to be confirmed)
- Webpack is used to bundle a javascript application and all its dependencies
- Webpack is also used to automate transpilation steps before final bundling, using plugins
- However, everything could be done manually, or using a script. But then it would be hard to have a watch strategy. And the full rebuild is quite expensive.
- Transpilation is not type checking, these are different processes
- eslint can be used to enfore coding standard in javascript and typescript
- Prettier can be configured to follow eslint standard

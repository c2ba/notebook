# Dev / Software Craftmanship

## Testing

- ✅ [Unit Testing vs Functional Testing: A Detailed Comparison](https://www.simform.com/unit-testing-vs-functional-testing/) ⏳ 9m

**Reading List**

- [Building a Successful Automated Functional Testing Strategy](https://www.simform.com/automated-functional-testing/) ⏳ 8m
- [Why Most Unit Testing is Waste - Python Testing](https://pythontesting.net/strategy/why-most-unit-testing-is-waste/)

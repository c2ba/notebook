# Dev - Sofwtare Development Patterns

This note summarize some patterns I saw, or reflected about, during my dev carrier.

## Execution context and configuration

A software execution can be be seen in two ways:
- contextualized
- uncontextualized

A software takes inputs and produce outputs. Even complex scenariis can be thought as input to output.

In the simplest case, the soft takes input on its command line or std input, and produce output on its std output.

A more complex case is interactivity: input is given as user inputs, output is given as GUI, for example. But even in this case we can formalize the input as a stream of events distributed in time, and the output as a stream of images (GUI) distributed in time also.

The execution context of a soft describe a configuration for the software when it is launched, and sometimes during its execution (the stream of input events could be seen as contextual input data).

When you create a software with some configuration, it could be great to define several levels of contexts that can be overrided by the execution environment.

Here is a list of layers:
- software layer
- system layer
- per environment layer
- per user layer
- multiple contexts per user
- per project layer
- per user per project layer
- environment variables layer
- command line arguments layers

The software layer defines the base configuration of the software: default values for all configuration variables. If may also set some variables as undefined, which would produce a runtime error at launch if the subsequent layers do not define them. For exemple, a software using **encrypted configuration** might require an encryption key, which cannot be set by default because it would be used to decrypt other configuration variables.

ConfigVariable(key, value, decrypt_key=Optional[str]) -> if decrypt_key is not None, read the decrypt key and use it to read this value

The system layer defines the configuration at the system level. Most often the system is a type of deployment environment (e.g. personal computer, cloud instance, smartphone, etc), and it factorize the configuration that could be applied to multiple environments of the same kind.

Per environment layer: specify for a specific environment de conf. It is where we would define encryption keys for a cloud deployed sofwtare.

Per user layer: for multi user systems. On a personal computer, often stored in home directory. On a cloud deployment, could be stored in database.

Multiple contexts per user. For some systems it makes sense to allow the user to easily switch between different sets of configurations. In that case the user could define a base config and override them accross different contexts, possibly with multiple levels. A good exemple is `kubectl`, where we can define several contexts in `.kube/config` and switch between them using `kubectl use-context NAME`.

Note that in that case, the `context` concepts makes perfect sense: we can switch contexts in a given session (the console).

Per project layer. Some systems acts on a given current project (which itself is part of the current execution context). A good exemple is `git`, where the current project is the current working directory. Inside this directory, we can have per project config layer: `.gitignore` and `.git` for example.
Another exemple: VSCode, where `.vscode/settings.json` overrides user settings (that itself override system settings).

Per user per project layer. In a given project, we can have per user settings overriding the project settings. Git allow this with `--local` config variables, which are stored for the project but not synced with other people. However, a good system would provide a way to share user configuration variables with other, with possibility of selection. We can even imagine a system where you would sync at the project level some encrypted configuration, allowing you to keep conf in sync for yourself accross several system on a given project, but still keeping it secret (or shared with other people to which you provide your secret key).
A good system would even allow you to define a system default config that you could apply as per user per project, but selectively override some.

Exemple: { configA: { use: system } } If defined in a project for the user, use system config instead of project

Environment variables layer: In general, the software will privilege environment variables over configuration variables, allowing to customize a given execution using env var def:

ENV_VAR=x ./run_the_soft

A good way of doing this would be to use inside the code: context.config_var = os.environ.get('software.config_var', get_from_config('config_var')) where get_from_config() would try to read from other layers.

Some systems could use env var to allow override: For exemple, `soft use-context X` could read context X from other layers, and set env vars from it. Then further executions of soft would use these env vars.

Command line layer. The final layer, that would be prioritized over any layer.

TODO: write a generic software configuration library.

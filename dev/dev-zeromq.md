# Dev / ZeroMQ

Guides:
- https://zguide.zeromq.org/
- https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmq.html

- ZeroMQ is an efficient messaging library
- It can be used for network process communication, inter process communication, and inter thread communication
- Since v3.3, ZeroMQ has a socket option called ZMQ_ROUTER_RAW that lets you read and write data without the ZeroMQ framing, so we can write communication with arbitrary protocols using ZeroMQ (http for example)

Fun projects ideas:

- Write a http server with ZeroMQ
- Write a multi-threaded path tracer with communication between threads using ZeroMQ
- Write a multi-threaded compute graph evaluator

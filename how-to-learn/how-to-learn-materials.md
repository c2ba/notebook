# How to Learn / Materials

- [Learning How to Learn: Powerful mental tools to help you master tough subjects | Coursera](https://www.coursera.org/learn/learning-how-to-learn)
  - Famous free course
- 👀 [Learn In Public | swyx.io](https://www.swyx.io/learn-in-public/)

## Great examples of learning resources to take example from

- [Guides | Tania Rascia](https://www.taniarascia.com/guides)
- [Interneting Is Hard | Web Development Tutorials For Complete Beginners](https://www.internetingishard.com/)

# How to Learn / Personal Notes

**Stuff**

- Subject -> Concepts -> Practices
  - A given **subject** covers **concepts** to learn, which can be achieved through **practice**.
- Paradox: You learn something once, don't practice it for a long time, then when you have to come back to it you are afraid. More afraid than if you never practiced it. I think this is due to the fear that we would have forget everything. But the brain is strong, have faith in it !
- Large Subjects vs Specific Subjects vs Concepts
  - Example: Computer Science (Large Subject) > Software Development (Large Subject) > Web Development (Large/Specific Subject) > Front-end web development (Specific Subject) > Bundling (Concept) > Webpack (Concept)
  - I would say:
    - A large subject is something that is *almost* impossible to learn completely, because it is too large for a lifestime, and constantly growing
    - A specific subject is more stable but also grows. You can become a specialist but it will take time. However you can focus on specific concepts of the subject for mastery, and only have knowledge of the existence of others and still be considered on expect. A specific subject would be a subject in which you can effectively apply the 80/20 rule with a strong focus on essential concepts.
    - A concept would be something with clear boundaries at a given instant in time. It can evolve quite fast (like computer technologies), but at a given moment it is possible to completely learn and master the concept. A specific technology, a small enough mathematical theory, some parts of science, a given skill, all of these are concepts. Most concepts can be split into sub-concepts, but it is common that a given sub-concept does not make sense on its own. I think that focusing on concepts for learning is a great way of achieving mastery at the subject level.
- First identify the state of the art of a specific subject. It will serve to identify concepts to learn first.
- Why reading tutorials ?
  - Improve / Reinforce own knownledge
  - Find great learning gems for others
  - Have fun

**Collection paradigms, and how to facilitate**

- Collect `notions`: Highlight in online resources
- Collect `reflexions`: Annotate `notions`, link them togetger
- Collect `ideas`: Create note from a thought

- Collect `reference item`: Link to an online resource
  - Collect before: usually to read later, subject seems interesting
  - Collect after: article was interesting, `notions` were collected, It can be used for future/current `action items`. It should be put under a `subject`.

**Collecting items and acting on these items**

Collecting items online to learn a subject can be done in two ways:

- Randomly, you find something that *seems* interesting
- On purpose, you dedicate a session to explore a subject, so you search

However, this is not so black and white. For example, you might do a tutorial session to explore something new, but the tutorial gives many links to explore and you end up in random collection phase.

Is this bad for learning ? I must admit that I don't know, I learn a lot of things by just collecting stuff online and reading pieces of it. I collect and say "I'll come back later to read it in details", but it's not that often that I come back. I keep collecting however, and often I find the same resource muliple time, so I read other parts of it and ends up learning the whole subject. But is it efficient ? I don't know (and I'm not sure I should care, as long as I learn).

However, a good side effect of massive collection is that you build a mental state of the art of the subject. You see concepts appearing, things that links together, stuff that is mentionned more often, etc. At the beginning concepts are empty in your mind, but as time passes and collection keeps occuring, you fill the missing knowledge about specific concepts. At some point you start to have a clear idea of what you actually learned.

Still, it remains messy, and hard to put you to the test. To assert that you learned something well:

- Identify key skills to master
- Do some projects requiring these skills

If the goal is the project, you need to use what you already know and be productive.

If the goal is learning, you need to allow yourself to be slow, and collect some new knowledge about what you don't know.

The project is used to discover what you don't know: at some point you will be blocked, or you will know some way to do something, but with the idea that there must be a better way.

**Discovering what you don't know**

- Watch
- Projects

**Technology learning**

Apply the ["golden circle"](http://www.moonseven-editing.com/golden-circle-content-marketing/) from Simon Sinek, in reverse:

When meeting a new technology, steps:
- The **why**: Why does this technology exists ? What needs does it fill ? If you don't have the problems the technology solves, don't use it (in real projects, you can still learn our of curiosity).
- The **how**: How to use the technology in various context. From scratch context, and integrated with other technologies. What are the building blocks to put in a project to integrate the technology ?
- The **what**: What can you build from the practices identified in the how.

**Active / Passive Learning**

- Doing one tutorial is not enough
- Reading one article is not enough
- Repetition is key
  - Both active repetition (practice) and passive repetition (consuming)

So how to learn something and iterate on it ?

- Often the knowledge of the existence of something to learn happens more or less randomly
  - However, with technology watch of specific subjects, it is more likely to discover new things to learn in relation with that subject
- Once having something new to learn start with a few tutorials (or book + exercises) and do them
  - Ideally these tutorials should start low level enough
  - Too high levels means a lot of things will be not understand
  - Understanding is key for a concept to stick in mind
  - But too low level means spending too much time on details
  - A possibility is to find several tutorials with different levels of abstraction
- After having done a few tutorials, if you don't practice you will have hard time to remember
  - Most of the practice comes from work or projects
  - If you cannot practice a subject regularly but still want to keep it in your mind, do active watch. It will serve to keep your mind remember things you learned
- Read other tutorials about the subject, but don't necessarily do them yourself
  - Once you have enough practice, a lot of things will overlap
  - However it is important to see many variations of the same thing

**Learning Path and Roadmap**

A learning roadmap would be a list of actions to take to learn a subject. A typology of these actions would be:

- *consuming action*: read, watch, listen to some resource about the subject
- *practice action*: do an exercice or a tutorial
- *production action*: create something new using concepts of the subject
- *teaching action*: teach concepts to someone else

Some actions can have several types. For example, doing a tutorial would be both a *consuming action* and a *practice action*. Doing an exercise is *practice action* unless the solution is consumed.

Writing a tutorial would be both a *production action* and a *teaching action*, especially if you get feedback from people and use it to improve the tutorial.

A learning path would be an already asserted and effective way of learning a subject. Examples of learning paths are books table of contents, formation plans or university course curriculums.

**Assert Learning**

Asserting learning at a personal level is hard:

- You don't know what you don't know
- Only way do know is active watching of a subject
- You don't have grades, only personal experience

A possible way of asserting knownledge is to take the subject under consideration and split it into concepts. Then for each concept, define a list of skills that should be mastered.

**Learning by teaching**

It is well known that teaching a subject to someone allows to reinforce learning of that subject.

So a good item to include in any learning path is **writing a tutorial about the subject or a concept of the subject**. What's great is that this item could be split into several small items scattered accross the learning path.

**How to write a tutorial**

Having a methodology to write a tutorial can be great to simplify doing it.

Tania Rascia writes on her blog several guides having a good basic shape: https://www.taniarascia.com/guides

Here is an example:

- A catch phrase, explaining what the guide is about
- An introduction, explaning in more detail what the guide is about
- A prerequiste list, essential to know if you can follow the guide. Each prerequiste redirects to a resource to learn the prerequiste. I think it is important to put only one, well known and currated.
- Goals, what should be learned and done at the end of the tutorial
- Content, the table of content
- The tutorial
- A conclusion, to wrap things up


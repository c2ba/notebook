# Open Source / Learning Materials

## 💎 [Seneca-CDOT/topics-in-open-source-2020: Topics In Open Source 2020](https://github.com/Seneca-CDOT/topics-in-open-source-2020)

A complete course introducing the "technological, social, and pragmatic aspects of developing open source software through direct involvement in large open source projects."

Check the [Wiki](https://github.com/Seneca-CDOT/topics-in-open-source-2020/wiki).

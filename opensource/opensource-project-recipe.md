# How to start an open source development project

> WIP: this note is work in progress

## Introduction

In this note I try to summarize steps to start an open source development project.

## Steps

- README
- LICENSE
- Contribution Guide
- issue template
- merge request template
- pre-commit to enforce conventions
- choose a commit convention such as https://www.conventionalcommits.org/en/v1.0.0/
- changelog generation
  - don't use commit message, but generate the changelog from individual files
  - https://keepachangelog.com/en/1.0.0/
- git workflow
- versionning tools
  - https://semver.org/
- CI/CD
  - CI: check rules are met
  - CD: release on the git platform (gitlab releases, github releases)
- unit tests
  - code coverage report
- developer environment setup
  - Dockerfile, docker-compose
  - Or a script
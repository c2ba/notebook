# Open Source / Tools

## Actively used

- scoop
- wallabag
- blender
- krita
- vscode
- pipx
- sumatrapdf
- yapa (pomodoro)
- calibre
- keeweb, keepass
- obs-studio
- brave (chrome)
- git
- git-journal

## I want to use

- zerotier https://www.zerotier.com/ (not in scoop, to include ?)
- zeronet https://zeronet.io/

## I want to investigate

### Keepass

- Learn the format, how to encode/decode it, what does it store
- Implement a server password
- Implement self hosting, cloud hosting, multi sync
- Implement chrome extension / deep integration of password fillter that connect to the server password
- Or just read the password file at low level, like the keepass program would do

Note from https://keepass.info/help/base/faq.html:
> KeePass 2.x already supports storing databases on servers using HTTP/FTP.

Multi-user capabilities: https://keepass.info/help/base/multiuser.html

### Git

- https://fabiensanglard.net/git_code_review/diff.php
- https://blog.jcoglan.com/2017/02/12/the-myers-diff-algorithm-part-1/
- https://shop.jcoglan.com/building-git/
- https://link.springer.com/article/10.1007/s10664-019-09772-z

- [Export open Github issues for offline use](https://gist.github.com/adam-p/9255324)

### VS Code

- Tutoriels about writing extensions

### Python

- The book about CPython

# Personal Development / Mind Palace
*See hypothesis notes*

## Terminology

- [Idée — Wikipédia](https://fr.wikipedia.org/wiki/Id%C3%A9e) ⏳ 3m

> Le terme idée évoque « ce que l'esprit conçoit ou peut concevoir, [...] tout ce qui est représenté dans l'esprit, par opposition aux phénomènes concernant l'affectivité ou l'action »

> Au sens contemporain, une idée est une représentation mentale. Naïvement, on peut dire que l'idée est dans la tête de celui qui pense. Elle n'a donc pas d'existence propre, en dehors de l'esprit qui la pense. Elle fait partie du monde intérieur, par opposition au monde extérieur des choses que l'on peut percevoir par nos sens.

> En revanche, selon Platon, les Idées ne sont pas dépendantes de celui qui les pense.

- [Notion — Wikipédia](https://fr.wikipedia.org/wiki/Notion)

> Une notion est une connaissance ; le reflet, dans l'esprit, d'objets réels et de phénomènes dans leurs caractéristiques et relations essentielles. C'est une connaissance élémentaire, souvent tirée d'observations empiriques. Elle est donc moins élaborée et abstraite que le concept. Cela s'applique essentiellement en philosophie.

## Reading list

- [Esprit — Wikipédia](https://fr.wikipedia.org/wiki/Esprit) ⏳ 10m
- [Concept (philosophie) — Wikipédia](https://fr.wikipedia.org/wiki/Concept_(philosophie)) ⏳ 12m
- [Imagination — Wikipédia](https://fr.wikipedia.org/wiki/Imagination) ⏳ 2m
- [Affectivité — Wikipédia](https://fr.wikipedia.org/wiki/Affectivit%C3%A9) ⏳ 1m

> L’affectivité est le large domaine de la vie de l'esprit auquel appartiennent les états ayant un aspect bon ou mauvais : sensation, émotion, sentiment, humeur (au sens technique d’état moral : déprime, optimisme, anxiété…).

# Recipes from 95degres.com

- https://www.95degres.com/recipes/03-12-2020-veloute-de-chou-fleur-a-la-cacahuete

## [Cake jambon cru chou-fleur brocoli](https://www.95degres.com/recipes/26-11-2020-cake-jambon-cru-chou-fleur-brocoli)

- Remplissez un tiers de la cuve du Vitaliseur d’eau et portez à ébullition.
- Déposez sur le tamis du Vitaliseur les bouquets de chou-fleur et de brocolis et faites-les cuire 4 minutes.
- Mélangez la farine, la fécule, le bicarbonate, la poudre d’amande, la poudre à lever, le curry, le sel et le poivre.
- Dans un autre bol mélangez les œufs, l’huile, le lait et le vinaigre.
- Incorporez les deux préparations et mélangez jusqu’à l’obtention d’une pâte homogène.
- Au fond du moule huilé déposez le fromage de chèvre, les légumes, la chiffonnade de jambon cru coupée en morceaux. Réservez quelques légumes et un peu de jambon.
- Versez la préparation puis déposez harmonieusement les légumes et le jambon réservé.
- Décorez le dessus de ciboulette hachée et faites cuire 50 minutes sur le tamis du Vitaliseur. Laissez-le refroidir avant de démouler.

Préparation : 10 min
Cuisson : 55 min

**INGRÉDIENTS**
pour 6 à 8 personnes

- 150 g de farine de riz
- 30 g de fécule de maïs
- 1 c. à c. de bicarbonate de sodium
- 1 sachet de poudre à lever sans gluten
- 2 c. à s. de poudre d’amande
- 1 c. à s. de curry
- 3 œufs
- 5 cl d’huile d’olive + pour le moule
- 10 cl de lait d’amande
- 100 g de fromage de chèvre (coupé en dés)
- 3 tranches de chiffonnade de jambon cru
- 150 g de chou-fleur
- 150 g de brocoli
- 1 c. à c. de vinaigre de cidre
- Quelques brins de ciboulette
- QS de sel et de poivre

# DevOps / Learning Materials

## Learning Path

### Important questions to answer / things to do and document

- How to dev and deploy a docker application (single container)
- How to dev and deploy a docker-compose application (multi container)
- How to dev and deploy a kubernetes application (multi container)
  - Locally: Docker for Desktop provide a cluster
  - Remotely: Can be tested on local network
  - Cloud: Try on various cloud providers
- How to administrate a Kubernetes Orchestrator for multiple clusters
  - One cluster = one application infrastructure
    - Usually one or several front-end, a DB, a few workers/queues
- How to handle termination of containers
  - Kill signal ?

## Docker

### Concepts

- [Autour des conteneurs : Qu'est-ce qu'un conteneur ? - Publicis Sapient Engineering - Engineering Done Right](https://blog.engineering.publicissapient.fr/2020/02/24/autour-des-conteneurs-quest-ce-quun-conteneur/) ⏳ 9m
- [Autour des conteneurs - Docker build et ses layers : docker en tient une couche ! - Publicis Sapient Engineering - Engineering Done Right](https://blog.engineering.publicissapient.fr/2020/03/26/autour-des-conteneurs-docker-build-et-ses-layers-docker-en-tient-une-couche/) ⏳ 11m
- [Docker Layers Explained - DZone Cloud](https://dzone.com/articles/docker-layers-explained) ⏳ 10m

- [Windows container networking | Microsoft Docs](https://docs.microsoft.com/en-us/virtualization/windowscontainers/container-networking/architecture) ⏳ 3m

### Cookbook

- [How to Get A Docker Container IP Address - Explained with Examples](https://www.freecodecamp.org/news/how-to-get-a-docker-container-ip-address-explained-with-examples/) ⏳ 6m
- [GitHub - borekb/docker-path-workaround: Path conversion workaround for Docker on Windows &amp;  Git Bash / MSYS2](https://github.com/borekb/docker-path-workaround) ⏳ 3m

### CI

- [Using Docker-in-Docker for your CI or testing environment? Think twice.](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/) ⏳ 9m

## Docker Alternatives

- [It's Time to Say Goodbye to Docker - DEV](https://dev.to/martinheinz/it-s-time-to-say-goodbye-to-docker-386h) ⏳ 17m
  - Mention [Podman](https://podman.io/) ⏳ 31m
  - Mention [Buildah | buildah.io](https://buildah.io/) ⏳ 10m

## Azure CI

- [Clean builds with self-hosted Azure Pipelines and GitHub Actions - Paul Knopf](https://pknopf.com/post/2020-06-22-clean-builds-with-self-hosted-azure-pipelines-and-github-actions/) ⏳ 3m
- [Deploying Azure Pipelines agents as containers to Kubernetes | Julio Casal	](https://juliocasal.com/2020/01/14/deploying-azure-pipelines-agents-as-containers-to-kubernetes/) ⏳ 5m
- [Run Azure DevOps Private Agents in Kubernetes Clusters – mohitgoyal.co](https://mohitgoyal.co/2019/01/10/run-azure-devops-private-agents-in-kubernetes-clusters/) ⏳ 7m
- [Building autoscaling CI infrastructure with Azure Kubernetes | Andreas' Blog](https://blog.anoff.io/2019-10-autoscaling-ci-agent-with-azure-kubernetes/) ⏳ 11m
- [Run a self-hosted agent in Docker - Azure Pipelines | Microsoft Docs](https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/docker?view=azure-devops#linux) ⏳ 11m
- [GitHub - julioct/azure-pipelines-kubernetes-agents: Helm chart to deploy Azure Pipelines agents as containers to an Azure Kubernetes Service cluster](https://github.com/julioct/azure-pipelines-kubernetes-agents) ⏳ 4m

## Function as a Service

- https://docs.openfaas.com/ | Introduction - OpenFaaS

## Kubernetes

- [Deploy, Scale and Upgrade an Application on Kubernetes with Helm](https://docs.bitnami.com/tutorials/deploy-application-kubernetes-helm/) ⏳ 13m
- [Convert Kubernetes YAMLs into Helm Chart YAMLs | Jhooq](https://jhooq.com/convert-kubernetes-yaml-into-helm/) ⏳ 9m
- [Se simplifier Kubernetes avec Helm et les Charts - Zwindler's Reflection](https://blog.zwindler.fr/2018/02/06/se-simplifier-kubernetes-helm-charts/) ⏳ 14m
- [Vous avez cherché Kubernetes - Zwindler's Reflection](https://blog.zwindler.fr/?s=Kubernetes) ⏳ 1m
- [Kubernetes - Ressources utiles pour bien débuter - Zwindler's Reflection](https://blog.zwindler.fr/2020/05/25/kubernetes-ressources-utiles-pour-bien-debuter/) ⏳ 5m
- [Kubernetes avec RancherOS et RKE - partie 1 - Zwindler's Reflection](https://blog.zwindler.fr/2020/10/26/kubernetes-avec-rancheros-et-rke-partie-1/) ⏳ 16m

# Kubernetes

## Introduction

> Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management of containerized applications.

Can be deployed:
- on local machine
- on cloud
- on-premise datacenter
- use a managed Kubernetes cluster

## kubectl

- The CLI tool to interact with Kubernetes.
- You can use kubectl to deploy applications, inspect and manage cluster resources, and view logs.
- Can be installed with scoop on windows.

**Commands**

- `kubectl get nodes`
- `kubectl get all`
- `kubectl config view`
- `kubectl config get-contexts`
- `kubectl config use-context docker-desktop`

## Kubernetes Dashboard

Kubernetes does not come by default with a dashboard and you need to enable it with the following command:

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.1.0/aio/deploy/recommended.yaml
```

More details: https://github.com/kubernetes/dashboard

To access:
```
kubectl proxy
```

Then go to URL http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

## Local Kubernetes

- Learning environment

## [Kubernetes Concepts](https://kubernetes.io/docs/concepts/)

### Overview

#### What is Kubernetes ?

- A platform for managing containerized workloads and services
- Declarative configuration and automation
- Google open-sourced the Kubernetes project in 2014 (predecessor was Borg https://kubernetes.io/blog/2015/04/borg-predecessor-to-kubernetes/)
- "Kubernetes" means helmsman or pilot in Greek
- Going back in time
  - Traditional deployment era
    - physical servers
    - no resource boundaries between applications
    - do not scale
  - Virtualized deployment era
    - multiple VMs on a single physical server
    - isolation between VMs, more security
    - better scalability, reduce hardware costs
  - Container deployment era
    - share the operating system among the applications -> more lightweight
    - own filesystem, share of CPU, RAM, process space, ...
    - decoupled from the underlying infrastructure -> portable
- Advantages to Container based development:
  - Container image creation is lightweight compare to VM image (both to define and to run)
  - CI / CD : allow for reliable and frequent container image build and deploy
  - Decoupling apps from infra: creation images are build/release time rather than deploy time
  - Consistency accross dev, test and production environments
  - Focus on application: manipulate OS logical resources rather than virtual hardware
  - Enable easier micro-service development and deployment
  - Better resource isolation and utilisation
- Why Kubernetes ?
  - Provides a framework to run distributed systems resiliently
  - Service discovery and load balancing
    - expose a container using the DNS name or their own IP
    - able to load balance and distribute network traffic
  - Storage orchestration
    - able to auto mount local storages, public cloud providers, and more
  - Automated rollouts and rollback
    - describe the desired state, let kube change the actual state to the desired at a controlled rate
  - Automatic bin packing
    - provide nodes
    - tell how much CPU and RAM you need per container
    - let kube dispatch containers to your nodes for best use of resources
  - Self healing
    - retarts containers that fail
    - replaces containers
    - kills containers that don't respond to user defined healt checks
    - does not expose them until they are ready to serve
  - Secret and configuration management
    - store and manage sensitive information
    - deploy and update secrets and app config without rebuilding images
    - don't need to expose secrets in stack configuration
- What Kubernetes is not
  - Not a Platform as a Service
  - Default solutions for deployment, scaling, load balancing, logging, moniroting, alerting are optional and pluggable
  - Building blocks for building developer platforms
  - Does not limit the types of apps supported (stateless, stateful, data-processing, as long as it can run in a container its ok)
  - Does not deploy, does not build
  - Does not provide application level services as built-in services (middlewares, data-processing frameworks, databases, caches, cluster storages)
  - Does not dictate logging, monitoring or alerting solutions
  - Does not provide nor mandate a configuration language/system
  - Does not provide nor adopt any comprehensive machine configuration, maintenance, management, or self-healing systems
  - Is not a mere orchestration system: it eliminates the need for orchestration.


#### Kubernetes Components

![](https://d33wubrfki0l68.cloudfront.net/2475489eaf20163ec0f54ddc1d92aa8d4c87c96b/e7c81/images/docs/components-of-kubernetes.svg)

- Deploying Kubernetes gives you a **cluster**
- A **cluster** is a set of **worker machines** called **nodes**, with at least one worker node
- Worker nodes host **Pods**
- A **Pod** represents a set of running containers in your cluster
- The **control plane** manages the nodes and the pods in the cluster
- The **controle plane** is the layer that exposes the API and interfaces to define, deploy and manage the lifecycle of containers
- In production envs, the control plane usually runs across multiple computers
- Control Plane Components
  - makes global decisions about the cluster, detect and respond to cluster events (e.g. starting up a new pod when a deployment's replicas field is unsatisfied)
  - can be run on any machine in the cluster, but for simplicity are usually started on the same machine which do not run user containers
  - kube-apiserver
    - expose the Kubernetes API, front-end for the control plane
    - scales horizontally, by deploying more instances
  - etcd
    - Consistent and highly-available key value store used for all cluster data
  - kube-scheduler
    - watches for newly created Pods with no assigned node, and selects a node for them to run on
  - kube-controller-manager
    - runs **controller** processes
    - **controller**: control loop that watches a shared state of the cluster (using apiserver) and makes changes attempting to move the current state towards the desired state.
    - logically separate processes, but compiled into a single binary and run in a single process
      - node controller: alert and respond when nodes go down
      - replication controller: maintain correct number of pods
      - endpoints controller: joins services and pods
      - service account & token controllers: create default accounts and API access tokens for new namespaces
  - cloud-controller-manager
    - embeds cloud-specific control logic
    - lets you link your cluster into your cloud provider's API
    - separates out the components that interact with that cloud platform from components that just interact with your cluster
    - specific to your cloud provider
    - not present on premises, or in a learning environment
    - following controllers can have cloud provider dependencies:
      - node controller: checking the cloud provider to determine if a node has been deleted in the cloud after it stops responding
      - route controller: setting up routes in the underlying cloud infrastructure
      - service controller: creating, updating and deleting cloud provider load balancers
- Node Components
  - run on every node
  - kubelet
    - make sure that containers are running in a Pod
    - takes a set of PodSpecs that are provided through various mechanisms and ensures that the containers described in those PodSpecs are running and healthy
    - doesn't manage containers which were not created by Kubernetes
  - kube-proxy
    - network proxy
    - implement part of the **Service** concept
    - maintains network rules on nodes to allow communincation to your Pods from inside or outside of your cluster
    - uses the OS packet filtering layer if available, otherwise forwards the traffic itself
  - Container runtime
    - software responsible for running containers: docker, containerd, CRI-O, or any implementation of the Kubernetes [Container Runtime Interface](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-node/container-runtime-interface.md)
- Addons
  - uses kube resources to implement cluster features
  - Some selected addons
    - DNS
      - kube heavily rely on DNS, so all kube clusters should have a cluster DNS server
      - containers started by kube automatically include this DNS server
    - Web UI (dashboard)
      - general purpose web UI to manage applications running in the cluster
    - Container Resource Monitoring
      - record generic time series metrics about containers in a central database
      - provides a UI for browsing that data
    - Cluster-level Logging
      - save container logs to a central log store with search/browsing interface


#### The Kubernetes API

- lets you query and manipulate the state of API objects in Kubernetes
- kubectl and kubeadm use the API
- you can also use REST calls and client libraries
- OpenAPI v2
- json or protobuf (mainly for intra-cluster communication)
- Kubernetes stores the serialized state of objects by writing them into etcd
- Kubernetes support multiple API versions to make it easier to eliminate fields or restructure resource repr
  - Versioning is done at the API level
  - The API server may serve the same underlying data through multiple API versions
  - Kubernetes has designed the Kubernetes API to continuously change and grow
  - new API resources and new resource fields can be added often and frequently
  - Elimination of resources or fields requires following the API deprecation policy
- The Kubernetes API can be extended with custom resources and aggregation layers

#### Working with Kubernetes Objects

- Understanding Kubernetes Objects
  - persistent entities in the Kubernetes system
  - represent the state of your cluster
  - they describe:
    - what apps are running and on which nodes
    - available resources to those apps
    - policies (restart, upgrade, fault tolerance)
  - objects definition is your cluster's desired state, and kube work to keep this state
  - Object Spec and Status
    - spec: desired state, user defined
    - status: current state
  - Describing an object
    - With the API: using json
    - With kubectl: you can use yaml, which is converted to json by kubectl
    - Contains api version to use, kind of object, metadata (name, UID, namespace) and spec
    - `kubectl apply -f` takes a yaml file as input and apply it
- Kubernetes Object Management
  - several different ways to create and manage Kubernetes objects
  - A Kubernetes object should be managed using only one technique. Mixing and matching techniques for the same object results in undefined behavior.
  - Techniques
    - Imperative commands
    - Imperative object configuration
    - Declarative object configuration
  - Imperative commands
    - a user operates directly on live objects in a cluster
    - using kubectl subcommands
    - simplest way to get started or to run a one-off task in a cluster
    - provides no history of previous configurations
  - Imperative object configuration
    - the kubectl command specifies the operation (create, replace, etc.), optional flags and at least one file name
    - The file specified must contain a full definition of the object in YAML or JSON format
  - Declarative object configuration
    - a user operates on object configuration files stored locally, however the user does not define the operations to be taken on the files
    - Create, update, and delete operations are automatically detected per-object by kubectl
- Object Names and IDs
  - name is unique for a type of resource in a namespace, uid is unique across the whole cluster
  - Most resource types require a name that can be used as a DNS subdomain name as defined in RFC 1123
  - Some resource types require their names to be able to be safely encoded as a path segment (may not be "." or ".." and the name may not contain "/" or "%")
  - Kubernetes UIDs are universally unique identifiers (also known as UUIDs)
- Namespaces
  - A virtual cluster backed by a physical cluster
  - For environments with many users spread accross teams or projects
  - Scope for names. Name of resources need to be unique within a namespace
  - Namespaces cannot be nested
  - A way to divide cluster resources between multiple users using resource quota
  - If the need is just to separate slightly different resources (e.g. different versions of the same software, use labels instead)
  - Working with Namespaces
    - `kube-` prefix is reserved for Kubernetes system namespaces
    - Viewing namespaces: `kubectl get namespace`
    - Kubernetes starts with four initial namespaces: default, kube-system, kube-public, kube-node-lease
    - To set the namespace for a current request, use the `--namespace` flag
    - Set the namespace for all subsequent requests with `kubectl config set-context --current --namespace=`
  - nodes and persistentVolumes, are not in any namespace

kubectl rollout restart deployment name_of_deployment
kubectl scale deployment --replicas=0 my-dep

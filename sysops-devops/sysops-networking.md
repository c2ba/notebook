# SysOps / Networking

## Proxy & VPNs

- SSH proxy tunnel
  - https://www.linuxjournal.com/content/use-ssh-create-http-proxy
  - https://catonmat.net/linux-socks5-proxy
- Squid
  - http://www.squid-cache.org/
  - https://github.com/sameersbn/docker-squid
- Mitmproxy
  - https://docs.mitmproxy.org/stable/
  - https://borntocode.fr/mitmproxy-analyser-le-trafic-de-vos-applications-mobiles/
  - https://blog.heckel.io/2013/07/01/how-to-use-mitmproxy-to-read-and-modify-https-traffic-of-your-phone/
  - https://docs.mitmproxy.org/stable/concepts-certificates/
- Self-made
  - https://www.sipios.com/blog-tech/a-simple-reverse-proxy-in-python
  - https://catonmat.net/http-proxy-in-nodejs
- OpenVPN server
  - https://linuxhint.com/install-openvpn-arch-linux/
  - https://wiki.archlinux.org/index.php/OpenVPN
  - https://khacnam.net/install-configure-openvpn-server-on-arch-linux.html
  - https://www.grottedubarbu.fr/serveur-openvpn-5-minutes-docker/

- [We need to talk: Can we standardize NO_PROXY?|GitLab](https://about.gitlab.com/blog/2021/01/27/we-need-to-talk-no-proxy/) ⏳ 11min
